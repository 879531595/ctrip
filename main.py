import requests
from websocket import create_connection
import re
import json
import base64
try:
    import thread
except ImportError:
    import _thread as thread

'本地websocket client端实例化，用于发送数据至node端解码'
local_websocket = create_connection("ws://127.0.0.1:8088/")
'Decrypt是解密函数，使用条件是local_websocket实例化，意思就是得和node端的websocket保持链接状态'


def request_ws(_key):
    local_websocket.send(_key)  # 发送数据
    result = local_websocket.recv()  # 接收数据
    return result



url = request_ws("check")

response = requests.get(url, verify=False)
sour = response.text
_obj = re.search('\((.*?)\)$',sour, re.S | re.I)

info_txt = _obj.group(1).strip()

json_info = json.loads(info_txt)
result = json_info.get("result")
risk_info = result["risk_info"]
process_value = risk_info["process_value"]
big_image = process_value.get("big_image")
small_image = process_value.get("small_image")

buffer = base64.b64decode(small_image)
with open("a.jpg", "wb") as fp:
    fp.write(buffer)


















