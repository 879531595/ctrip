import requests
import io
from websocket import create_connection
import re
import json
import base64
import time
from aip import AipOcr
from imgutil import img_stlip_and_convert
import os
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt


try:
    import thread
except ImportError:
    import _thread as thread

import pygame
pygame.init()
data_dir = 'imgdb'
if not os.path.exists(data_dir):
    os.mkdir(data_dir)

OUTDIR = os.path.join(os.path.dirname(__file__), "out")
if not os.path.exists(OUTDIR):
    os.makedirs(OUTDIR)


keywords_path = os.path.join(os.path.dirname(__file__), "keywords.json")
if not os.path.exists(keywords_path):
    with open(keywords_path,"w") as fp:
        fp.write('')



""" 你的 APPID AK SK """
APP_ID = '16937964'
API_KEY = 'tPTu4w5A33Quyqyik8cGQlvT'
SECRET_KEY = 'jdj55LxCZZ6LzXOCV3YLxWzKNQmsaBYE'

""" 如果有可选参数 """
options = {
    "language_type": "CHN_ENG",
    "detect_direction": "true",
    "detect_language": "true",
    "probability": "true"
}
keywords = []
client = AipOcr(APP_ID, API_KEY, SECRET_KEY)

imgs_num = 2

'本地websocket client端实例化，用于发送数据至node端解码'
local_websocket = create_connection("ws://172.18.15.135:8088/")
'Decrypt是解密函数，使用条件是local_websocket实例化，意思就是得和node端的websocket保持链接状态'

def generate_img(words):
    for word in words:
        font = pygame.font.Font("CNganKaiHK-Bold.ttf", 50)
        # 当前目录下要有微软雅黑的字体文件msyh.ttc,或者去c:\Windows\Fonts目录下找
        # 64是生成汉字的字体大小
        rtext = font.render(word, True, (0, 0, 0), (255, 255, 255))
        pygame.image.save(rtext, os.path.join(data_dir, word + ".png"))
    print("img generate successed")

# OCR
def baiduOCR(bimage):
    """ 带参数调用通用文字识别, 图片参数为本地图片 """
    info = client.basicGeneral(bimage, options)
    if "words_result" in info:
        words_result = info["words_result"]
        words_result = words_result[0]
        if "words" in words_result:
            words = words_result.get("words")
            return words

    return "null"



    # 获取图片url
def request_ws(_key):
    local_websocket.send(_key)  # 发送数据
    result = local_websocket.recv()  # 接收数据
    return result


class CreateImgsTable():

    def __init__(self, imgs_num):
        self.imgs_num = imgs_num
        pass

    def run(self):
        index = 1
        while index <= self.imgs_num:
            try:
                print("started ORC, index:", index)
                url = request_ws("check") # 获取链接
                response = requests.get(url, verify=False)
                sour = response.text
                if '"process_type":"SELECT"' not in sour:
                    continue
                else:
                    _obj = re.search('\((.*?)\)$', sour, re.S | re.I)
                    info_txt = _obj.group(1).strip()
                    json_info = json.loads(info_txt)
                    result = json_info.get("result")
                    risk_info = result["risk_info"]
                    process_value = risk_info["process_value"]
                    big_image = process_value.get("big_image")
                    small_image = process_value.get("small_image")
                    small_buffer = base64.b64decode(small_image)
                    big_buffer = base64.b64decode(big_image)

                    word = baiduOCR(small_buffer)  # 使用百度接口识别
                    if word not in keywords:
                        keywords.append(word)
                    else:
                        continue

                    if "null" != word:
                        small_out_path = os.path.join(OUTDIR, "small_%s.jpg" % word)
                        small_save_path = os.path.join(data_dir, "small_%s.jpg" % word)
                        with open(small_out_path, "wb") as fp:
                            fp.write(small_buffer)

                        generate_img(word)
                        big_out_path = os.path.join(OUTDIR, "big_%s.jpg" % word)
                        big_save_path = os.path.join(data_dir, "big_%s.jpg" % word)

                        img_stlip_and_convert(big_buffer, big_save_path, False)

                        with open(big_out_path, "wb") as fp:
                            fp.write(big_buffer)
                    time.sleep(2)
            except Exception as ex:
                print("Exception in run", str(ex))


            index += 1
            print("finished ORC, index:", index)


        with open(keywords_path,"w") as fp:
            json.dump(keywords, fp)
if __name__ == '__main__':
    cit = CreateImgsTable(1000)
    cit.run()



















