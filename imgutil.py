import cv2 as cv
import os
import io
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

def img_stlip_and_convert(buffer, out_path, stlip_flag=False):
    byte_stream = io.BytesIO(buffer)
    img = Image.open(byte_stream).convert('L')
    # img = Image.open(path)
    if stlip_flag:
        img = img.crop((13, 13, 108, 31))
    out_img = np.array(img)
    rows, cols = out_img.shape
    for i in range(rows):
        for j in range(cols):
            if (out_img[i, j] < 100):
                out_img[i, j] = 0
            else:
                out_img[i, j] = 1


    plt.imshow(out_img, cmap='gray')
    plt.axis('off')
    plt.savefig(out_path)
    print("img save succesed")




